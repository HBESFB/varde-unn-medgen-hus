process Encrypt {
  // Encrypts files with a private gpg key
  // Encrypted output should have a timestamp
  publishDir(
        path: "output/",
        mode: 'copy',
    )

  input:
  val ready
  path inputfile
  path pubkey
  val pipeline
  val pass

  output:
  path "${inputfile}.gpg", emit: gpg

  shell:
  '''
  #gpg --encrypt --recipient !{pubkey.getSimpleName()} --batch --always-trust !{inputfile}
  gpg -u !{pipeline} -s -e -r !{pubkey.getSimpleName()} --batch --always-trust --yes --passphrase !{pass} --pinentry-mode=loopback !{inputfile} > !{inputfile}.gpg
  '''
}
