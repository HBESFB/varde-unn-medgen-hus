process Import_Gpg_Pub_Key {
  // debug true

  input:
  val ready
  path pubkey

  output:
  val true

  shell:
  '''
  # Import the public key
  gpg --import !{pubkey}
  '''
}
