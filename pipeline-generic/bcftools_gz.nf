process Bcftools_Gz {

  input:
  path inputfile

  output:
  path "${inputfile}.gz", emit: gz
  path "${inputfile}.gz.csi", emit: csi


  shell:
  '''
  bcftools view !{inputfile} -Oz -o !{inputfile}.gz
  bcftools index !{inputfile}.gz
  '''

  stub:
  """
  touch ${inputfile}.gz ${inputfile}.gz.csi
  """
}
