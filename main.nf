#!/usr/bin/env nextflow
// DSL 2 support and thread configuration.
nextflow.enable.dsl=2
include {Debug} from './pipeline-generic/debug.nf'
include {Encrypt} from './pipeline-generic/encrypt.nf'
include {Decrypt_Vcf} from './pipeline-generic/decrypt_vcf.nf'
include {Decrypt_Json} from './pipeline-generic/decrypt_json.nf'
include {Import_Gpg_Priv_Key} from './pipeline-generic/import_priv_key.nf'
include {Import_Gpg_Pub_Key} from './pipeline-generic/import_pub_key.nf'
include {Download_Input} from './pipeline-generic/download_input.nf'
include {Bcftools_Merge} from './pipeline-generic/bcftools_merge.nf'
include {Bcftools_Gz} from './pipeline-generic/bcftools_gz.nf'
include {Pedant} from './pipeline-generic/pedant.nf'

// Mandatory: 
// params.pipline is set by --pipeline <shorthand_id> (This parameter is mandatory. See pipeline overview in README.md) 
// params.gpg is the relative path to Public GPG key directory, which is present in repo. This should contain all relevant GPG-keys (referenced by shorthand id)
// params.input is the default input directory
params.pipeline=null
params.gpg="public_gpg_keys"
params.inputdir="input"

// Mandatory: 
// Set --decrypt=<priv_gpg_key>, and --decrypt_pass=<password> which is needed to sign outputfiles, and decrypt input if neccessary.
params.decrypt=null
params.decrypt_pass=null

// Optional:
// If --repo=<priv_ssh_key> is set, inputfiles will be downloaded from "Varde-local-input" in folder "input/<pipeline>", a private gitlab repository with encrypted input (prodution files)
// If not, params.input will be used and defaults to "$PWD/input" (can be used by docker mounts)
params.repokey=null
params.repodir=null

// Optional:
// If inputfiles are local and not encrypted, set this to false
params.encrypted=true

// Optional:
// If debug is set to True, output from workflows, which should be unencrypted, is written to params.outputdir
params.debug=false

// Include available pipelines to run. Add more pipelines here in the future.
// Other workflows can be added by including pipeline below, and adding it with an elif-clause in workflow{}. 
include {UNN_MedGen_Alamut} from './pipeline-unn-medgen/alamut.nf'
include {UNN_MedGen_Alissa} from './pipeline-unn-medgen/alissa.nf'
include {HUS_MGM}           from './pipeline-hus-mgm/hus.nf'

workflow{
  // Get all gpg pubkeys, select the appropriate one based on --pipeline shorthand ID.
  // Also, import the vardedb key, which is used for last stage encryption
    pubkeys = Channel.fromPath("${params.gpg}/*.asc")
    vardepgp = pubkeys.filter { it.getName() == 'vardedb.asc' }
    filtered = pubkeys.filter { file ->
        file.baseName.contains(params.pipeline)
    }
    pgp = filtered.first()

  // Reference genome value channel
    hg19 = Channel.value(file("${baseDir}/hg19/hg19.fa"))

    /*
    Institution specific processing from here on out
  */
    if (params.pipeline == "UNNAMG"){
        println("Running VARDE local workflow for UNNAMG (University Hospital of North Norway, UNN)")
	
        // Import private gpg for signing (and decrypt if input files are encrypted with local key)
        //Import_Gpg_Priv_Key(Channel.value(file(params.decrypt)), params.decrypt_pass)
    
    // Option 1: Inputfiles are encrypted and in connected GitLab Repo
    // Downloads and decrypts input from repo if params.repo is defined
    if (params.repokey && params.encrypted == true){
        Download_Input(params.repokey, params.pipeline, params.repodir)
        Import_Gpg_Priv_Key(Channel.value(file(params.decrypt)), params.decrypt_pass)
        vcf = Decrypt_Vcf(Import_Gpg_Priv_Key.out, Download_Input.out.vcf.flatten(), params.decrypt_pass)
        json = Decrypt_Json(Import_Gpg_Priv_Key.out, Download_Input.out.json.flatten(), params.decrypt_pass)
    }
    // Option 2: Inputfiles are local and encrypted
    // Decrypts input if params.decrypt is defined, but params.repo is undefined
    else if(params.encrypted == true){
        Import_Gpg_Priv_Key(Channel.value(file(params.decrypt)), params.decrypt_pass)
        vcf = Decrypt_Vcf(Import_Gpg_Priv_Key.out, Channel.fromPath("${params.inputdir}/*.vcf.gpg"), params.decrypt_pass)
        json = Decrypt_Json(Import_Gpg_Priv_Key.out, Channel.fromPath("${params.inputdir}/*.json.gpg"), params.decrypt_pass)
    }
    // Option 3: Inputfiles are local (Priv key needed for signing)
    // params.repo and params.decrypt are undefined
    else if (params.encrypted == false){
        Import_Gpg_Priv_Key(Channel.value(file(params.decrypt)), params.decrypt_pass)
        vcf = Channel.fromPath("${params.inputdir}/*.vcf")
        json = Channel.fromPath("${params.inputdir}/*.json")
    }
    
    // Process files
    UNN_MedGen_Alamut(vcf, hg19)
    UNN_MedGen_Alissa(json, hg19)

    // Generate local discrepancy report between the two Alissa files
    // merge process
    // bcftools isec -p dir -n=2 A.vcf.gz B.vcf.gz
    // - Generates: 0000.vcf and 0001.vcf (README.txt has info on original filenames)
    // Script that reports discrepancies

    // Merge two Alissa

    // Generate local discrepancy report between Alissa and Alamut
    // merge process
    // bcftools isec -p dir -n=2 A.vcf.gz B.vcf.gz
    // - Generates: 0000.vcf and 0001.vcf (README.txt has info on original filenames)
    // Script that reports discrepancies

    // Merge Alissa and Alamut

    /* Reusing processes: (Bcftools_Merge)
    include { ReUsable as ReUsable1 } from './modules/reusable'
    include { ReUsable as ReUsable2 } from './modules/reusable'
    */
    
    // Bcftools merge here
    outputfiles = UNN_MedGen_Alamut.out.vcf.concat(UNN_MedGen_Alissa.out.vcf)
    Bcftools_Gz(outputfiles)
    Bcftools_Merge(params.pipeline, Bcftools_Gz.out.gz.collect(), Bcftools_Gz.out.csi.collect())

    // Pedant here
    Pedant(Bcftools_Merge.out.vcf)

    // Encrypt files and publish
    Import_Gpg_Pub_Key(Import_Gpg_Priv_Key.out, vardepgp)
    Encrypt(Import_Gpg_Pub_Key.out, Bcftools_Merge.out.vcf, vardepgp, params.pipeline, params.decrypt_pass)
    // Publish unencrypted output to params.outputdir if params.debug==True
    if (params.debug) {
        Debug(outputfiles.concat(Bcftools_Merge.out.vcf))
    } 
    }
    else if (params.pipeline == "HUSMGM"){
        println("Running VARDE local workflow for HUS-MGM (Bergen)")
        HUS_MGM ()
    }
    else{
        println("Pipeline not recognized. See help for available pipelines");
    }
}