process Sort_Variants {
  // Sorts variants in vcf file from 1-23

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.sorted.vcf", emit: vcf

  shell:
  '''
  # Extract headers before sort
  cat !{inputfile} | grep '^#' > headers
  # Sort from 1-23
  cat !{inputfile} | grep -v '^#' | sort -h -k1 -k2 > variants
  # Concat header and variants
  cat headers variants > !{inputfile.getSimpleName()}.sorted.vcf
  '''
}
