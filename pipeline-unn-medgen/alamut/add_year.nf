process Add_Year_Null {
  // Adds a static list of contig=ID to headers

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.added_year.vcf", emit: vcf

  shell:
  '''
  # Extract ## headers. Change ID to uppercase
  cat !{inputfile} | grep '^#' > headers
  # Extract variants
  cat !{inputfile} | grep -v '^#' > variants_tmp
  # Append YEAR=0 to variants
  cat variants_tmp | sed 's/$/;YEAR=0/g' > variants
  # Concat ##headers and variants
  cat headers variants > !{inputfile.getSimpleName()}.added_year.vcf
  '''
}
