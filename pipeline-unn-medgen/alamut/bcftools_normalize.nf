process Bcftools_Normalize {

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.normalized.vcf", emit: vcf
  path "${inputfile.getSimpleName()}.normalized.log", emit: log

  shell:
  '''
  bcftools view !{inputfile} -Oz -o !{inputfile}.gz
  bcftools index !{inputfile}.gz
  bcftools norm -Ov --check-ref wx -f /varde/hg19/hg19.fna -m- !{inputfile}.gz -o !{inputfile.getSimpleName()}.normalized.vcf 2&> !{inputfile.getSimpleName()}.normalized.log
  '''
}
