process Filter_HG38 {
  // Removes potential HG38 variants, which should not be processed
  // echo true

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.hg38-filtered.vcf", emit: vcf

  shell:
  '''
  # Naive grep to filter out hg38 variants if present
  cat !{inputfile} | grep -v 'GRCh38'> !{inputfile.getSimpleName()}.hg38-filtered.vcf
  '''
}
