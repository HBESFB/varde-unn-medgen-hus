process Filter_50 {
  // Removes potential HG38 variants, which should not be processed
  // echo true

  input:
  path inputfile

  output:
  path "${inputfile.getSimpleName()}.50-filtered.vcf", emit: vcf

  shell:
  '''
  # Naive grep to filter out variants with >=50 bp if present
  awk '{ if ($1 ~ /^#/ || length($4) <= 50 && length($5) <= 50) print }' !{inputfile} > !{inputfile.getSimpleName()}.50-filtered.vcf
  '''
}
