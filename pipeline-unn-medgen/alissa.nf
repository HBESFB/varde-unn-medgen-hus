// Include modules for each tool
include {Convert_From_Json} from './alissa/convert_from_json.nf'
include {Convert_5ity} from './alissa/convert_5ity.nf'
include {Validate} from './alissa/validate.nf'
include {Sort_Variants} from './alissa/sort_variants.nf'
include {Filter_50} from './alissa/filter_50.nf'
include {Bcftools_Normalize} from './alissa/bcftools_normalize.nf'

// This current implementation takes only vcf files from Alamut as of now.

workflow UNN_MedGen_Alissa {
  take:
    inputfiles
    hg19

  main:
    // Use this template to add a stage
    // Template(inputfiles)

    // Convert json output from Alissa to VCF format
    script = Channel.value(file("${baseDir}/bin/alissa_convert_json_to_vcf.py"))
    Convert_From_Json(inputfiles, script)

    // 5-ity conversion from text to numbers
    Convert_5ity(Convert_From_Json.out.vcf)

    // Filter all variants longer than 50 bp
    Filter_50(Convert_5ity.out.vcf)

    // Sort from lowest to highest (chromosome)
    Sort_Variants(Filter_50.out.vcf)

    // Remove inconsistencies and other trash with bcftools
    Bcftools_Normalize(Sort_Variants.out.vcf)

    // Validate validates the final VCF
    Validate(Bcftools_Normalize.out.vcf, hg19)

    // Output
    emit:
      //Validate.out.vcf
      vcf = Validate.out.vcf
}
