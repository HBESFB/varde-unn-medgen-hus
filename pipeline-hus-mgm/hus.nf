
// Check mandatory parameters
if (params.alamut_vcf) { ch_alamut_vcf = file(params.alamut_vcf) } else { exit 1, 'alamut_vcf not specified!' }
if (params.alissa_json) { ch_alissa_json = file(params.alissa_json) } else { exit 1, 'alissa_json not specified!' }
if (params.outdir) { ch_outdir = file(params.outdir) } else { exit 1, 'Outdir not specified!' }
if (params.ref_fasta) { ch_ref = file(params.ref_fasta) } else { exit 1, 'Reference fasta not specified!' }

// Check input path parameters to see if they exist
def checkPathParamList = [ params.alamut_vcf, params.alissa_json, params.outdir, params.ref_fasta ]
for (param in checkPathParamList) { if (param) { file(param, checkIfExists: true) } }

include {Alissa_to_vcf}  from './modules/alissa_to_vcf.nf'
include {Convert_5ity}   from './modules/hus_convert_5ity.nf'
include {NormVt}         from './modules/normVt.nf'
// include {Bcftools_Merge} from '../pipeline-generic/bcftools_merge.nf'
include {Compare_internal} from './modules/compare_internal_vcfs.nf'
include {Discrepancy_check} from './modules/internal_discrepancy_check.nf'
include {Vcf_Concat} from './modules/vcf_concat.nf'
include {Bcftools_Gz}    from '../pipeline-generic/bcftools_gz.nf'
include {Pedant}         from './modules/pedant.nf'

workflow HUS_MGM {
    println("Running HUSMGM workflow")
	// TODO: import Gpg priv key
	// TODO: import Gpg Pub key
	// we don't need decryption of our input data like in the UNN workflow
	// this is because will be storing and running everything locally
	// however, we need the keys for encryption of results

	// ---- Alissa to vcf ----
    Alissa_to_vcf(Channel.fromPath(params.alissa_json).combine(Channel.fromPath(params.ref_fasta)))

    // 5-ity conversion from text to numbers
    Convert_5ity(Alissa_to_vcf.out.vcf)

    // Normalize and sort alissa variants 
	vt_input = Convert_5ity.out.vcf.combine(Channel.fromPath(ch_ref))
	NormVt(vt_input)

	Compare_internal(Channel.fromPath(params.alamut_vcf).combine(NormVt.out.vcf))

	// Check for discrepancies in class
	Discrepancy_check(Compare_internal.out.internal_discrepancies_class)
	
	// ----  Compare/merge Alissa and Alamut vcfs ----
	
    Bcftools_Gz(NormVt.out.vcf.concat(Channel.fromPath(params.alamut_vcf)))
    // Bcftools_Merge(params.pipeline, Bcftools_Gz.out.gz.collect(), Bcftools_Gz.out.csi.collect())
    Vcf_Concat(Discrepancy_check.out.ready, Bcftools_Gz.out.gz.collect(), Bcftools_Gz.out.csi.collect())

	// // --- Pedant for checking vcf formatting ----
    Pedant(Vcf_Concat.out, Channel.fromPath(params.ref_fasta))
	// TODO: Encrypt if pedant is ok
	emit:
		Vcf_Concat.out.vcf
}