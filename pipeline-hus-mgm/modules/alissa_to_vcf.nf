
process Alissa_to_vcf {
    input:
    tuple path(alissa_json), path(fasta)

    output:
    path "${alissa_json.getSimpleName()}.converted.vcf", emit: vcf

    script:
    """
    # Convert Alissa JSON to vcf using python script
    hus_alissa_to_vcf.py --json ${alissa_json} --fasta ${fasta} --output ${alissa_json.getSimpleName()}.converted.vcf
    """

    stub:
    """
    touch ${alissa_json.getSimpleName()}.converted.vcf
    """
}
