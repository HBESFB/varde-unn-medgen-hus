process Vcf_Concat {

  input:
  val ready
  path inputfiles
  path indexfiles

  output:
  path "${params.pipeline}-varde-${new Date().format('yyyyMMdd')}.vcf", emit: vcf
  
  script:
  """
  bcftools concat ${inputfiles} -O v -a | bcftools norm -m- | bcftools annotate -x  'INFO/ALAMUT_POS,INFO/ALISSA_POS,INFO/OLD_VARIANT' > intermediate.vcf
  # Add original headers to no_duplicates.vcf
  grep -E '^#' intermediate.vcf > no_duplicates.vcf
  # Sort and remove duplicates. Add them to no_duplicates.vcf 
  grep -Ev '^#'  intermediate.vcf | sort -V | cat -n | sort -b -V -uk2,8 | cut -f2- >> no_duplicates.vcf
  # Sort with bcftools to get X and MT in the correct order after the previous step
  bcftools sort -o "${params.pipeline}-varde-${new Date().format('yyyyMMdd')}.vcf" no_duplicates.vcf
  """
  stub:
  """
  touch "${params.pipeline}-varde-${new Date().format('yyyyMMdd')}.vcf"
  """

}
