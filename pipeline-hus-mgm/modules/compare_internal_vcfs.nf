process Compare_internal {
    input:
    tuple path(alamut), path(alissa)

    output:
    path "internal_discrepancies_class.csv" ,emit: internal_discrepancies_class

    script:
    """
    compare_internal_vcfs.R ${alamut} ${alissa}

    """
    stub:
    """
    touch internal_discrepancies_class.csv
    """
}
