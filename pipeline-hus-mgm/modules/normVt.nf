process NormVt {
    containerOptions = "--user root"
    publishDir "${params.outdir}", mode: 'copy'

    input:
        tuple path(vcf), path(ref) 

    output:
        path "*.normalized.vcf", emit: vcf

    script:
    """

    vt sort ${vcf} -o "sortvt.vcf" 
    vt decompose -s "sortvt.vcf" -o ${vcf.getSimpleName()}.decomposed.vcf
    vt normalize \
    -r ${ref} \
    ${vcf.getSimpleName()}.decomposed.vcf \
    -o "${vcf.getSimpleName()}.normalized.vcf"

    """
    stub:
    """
    touch stub.normalized.vcf
    """
}
