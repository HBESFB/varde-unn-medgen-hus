process Discrepancy_check {
    input:
    path(discrepancies)

    output:
    val true, emit: ready // ready signal to other processes

	//TODO: Allow year?
    script:
    """
    if [[ \$(wc -l < ${discrepancies}) -ge 2 ]];then
        echo "There are descripancies in the variant classifications, see process output for full list"
        exit 1
    fi
    if [[ \$(wc -l <${discrepancies}) -ge 2 ]];then
        echo "There are descripancies in the year of variant classifications, see process output for full list"
        exit 1
    fi
    """
    stub:
    """
    test -f $discrepancies
    """
}
