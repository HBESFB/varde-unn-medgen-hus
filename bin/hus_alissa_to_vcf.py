#!/usr/bin/env python3
import argparse
import json
from datetime import datetime
import pysam


def write_vcf(input_file: str, fasta: str, output: str) -> None:
    with open(output, "w") as vcf_file:
        # Write ## headers to the output file
        vcf_file.write("""##fileformat=VCFv4.4
##INFO=<ID=CLASS,Number=1,Type=String,Description="Variant classification">
##INFO=<ID=ACMG,Number=.,Type=String,Description="ACMG criteria">
##INFO=<ID=PUBMED,Number=.,Type=String,Description="List of Pubmed IDs">
##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Structural variant length">
##INFO=<ID=YEAR,Number=1,Type=Integer,Description="Year of interpretation">
##INFO=<ID=ALISSA_POS,Number=.,Type=String,Description="Original position in Alissa, chr:pos:ref/alt encoding">
##contig=<ID=1,length=249250621>
##contig=<ID=2,length=243199373>
##contig=<ID=3,length=198022430>
##contig=<ID=4,length=191154276>
##contig=<ID=5,length=180915260>
##contig=<ID=6,length=171115067>
##contig=<ID=7,length=159138663>
##contig=<ID=8,length=146364022>
##contig=<ID=9,length=141213431>
##contig=<ID=10,length=135534747>
##contig=<ID=11,length=135006516>
##contig=<ID=12,length=133851895>
##contig=<ID=13,length=115169878>
##contig=<ID=14,length=107349540>
##contig=<ID=15,length=102531392>
##contig=<ID=16,length=90354753>
##contig=<ID=17,length=81195210>
##contig=<ID=18,length=78077248>
##contig=<ID=19,length=59128983>
##contig=<ID=20,length=63025520>
##contig=<ID=21,length=48129895>
##contig=<ID=22,length=51304566>
##contig=<ID=X,length=155270560>
##contig=<ID=Y,length=59373566>
##contig=<ID=MT,length=16569>
#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO
""")

        # Process each input line
        with open(input_file, "r") as input:
            data = json.load(input)
            # Remove duplicates
            variants_no_dups = latest_interpretation(data["molecularVariants"])
            for variant in variants_no_dups:
                if variant["type"] == "insertion":
                    pos, ref, alt = handle_insertion(variant, fasta)
                    vcf_file.write(variant_string(variant, pos, ref, alt))
                elif variant["type"] == "deletion":
                    pos, ref, alt = handle_deletion(variant, fasta)
                    vcf_file.write(variant_string(variant, pos, ref, alt))
                else:
                    vcf_file.write(variant_string(variant))
            input.close()
        vcf_file.close()


def make_uid(variant: dict[str, any]) -> str:
    return hash(
        f"{variant['chromosome']}{variant['start']}{variant['stop']}{variant['reference']}{variant['alternative']}"
    )


def latest_interpretation(variants: list[dict[str, any]]) -> list[dict[str, any]]:
    lookup = dict()
    # Collect all interpretations per variant
    # A variant is identified by chromosome, start, stop, reference and alternative
    for variant in variants:
        uid = make_uid(variant)
        if uid in lookup.keys():
            lookup[uid].append(variant)
        else:
            lookup[uid] = [variant]

    # We want to keep one interpretation per variant
    # If there exists more than one, keep the newest
    latest_interpretation = []
    for variant_list in lookup.values():
        n_variants = len(variant_list)
        if n_variants > 1:
            newest = variant_list[0]
            newest_date = datetime.strptime(
                variant_list[0]["lastUpdatedOn"], "%Y/%m/%d %H:%M:%S"
            )
            for variant in variant_list[1:]:
                lastUpdatedOn = datetime.strptime(
                    variant["lastUpdatedOn"], "%Y/%m/%d %H:%M:%S"
                )
                if lastUpdatedOn > newest_date:
                    newest_date = lastUpdatedOn
                    newest = variant
            latest_interpretation.append(newest)

        elif n_variants == 1:
            latest_interpretation.append(variant_list[0])
        else:
            raise Exception("Something is really wrong")

    return latest_interpretation


def variant_string(
    variant: dict[str, any], pos: str = ".", ref: str = ".", alt: str = "."
) -> str:
    chrom = variant["chromosome"]
    info = variant["classification"]
    original_pos = variant["start"]
    original_ref = variant["reference"]
    original_alt = variant["alternative"]
    lastUpdatedOn = datetime.strptime(variant["lastUpdatedOn"], "%Y/%m/%d %H:%M:%S")

    if pos == "." and ref == "." and alt == ".":
        return f"{chrom}\t{original_pos}\t.\t{original_ref}\t{original_alt}\t.\t.\tCLASS={info};YEAR={lastUpdatedOn.year}\n"

    return f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info};YEAR={lastUpdatedOn.year};ALISSA_POS={chrom}:{original_pos}:{original_ref}/{original_alt}\n"


def handle_insertion(variant: object, fasta: str) -> tuple[str, str, str]:
    # We need to change the representation of variants with dot as reference allele
    if variant["reference"] == ".":
        # Create coordinates for fetching the nucleotide in reference position
        # Pysam fetch is 0-based, half-open intervals [0, n)
        # This means we need to subtract one, as our start position is 1-based
        start = int(variant["start"]) - 1
        end = int(variant["start"])
        # pos will not be different from the original representation
        pos = variant["start"]
        # Read the fasta file
        reference = pysam.FastaFile(fasta)
        # Fetch the nucleotide
        ref = reference.fetch(variant["chromosome"], start, end)
        reference.close()
        # Add the nucleotide to the original alternative sequence.
        alt = ref + variant["alternative"]
        return pos, ref, alt
    else:
        return variant["start"], variant["reference"], variant["alternative"]


def handle_deletion(variant: object, fasta: str) -> tuple[str, str, str]:
    # We need to change the representation of variants with dot as alternative allele
    # This is done by adding a nucleotide to the variant
    if variant["alternative"] == ".":
        # Create coordinates for fetching the nucleotide one position before the current start position
        # Pysam fetch is 0-based, half-open intervals [0, n)
        # This means we need to subtract two to the current 1-based position
        # To fetch the 0-based position for the nucleotide before the current ref
        start = int(variant["start"]) - 2
        end = int(variant["start"]) - 1
        # Pos is the new position of the variant in the reference genome
        # Since we are adding one nucleotide, we have to subtract one from the original position
        pos = str(int(variant["start"]) - 1)
        # Read the fasta file
        reference = pysam.FastaFile(fasta)
        # Fetch the nucleotide before our original variant
        # This is our new alternative allele
        alt = reference.fetch(variant["chromosome"], start, end)
        reference.close()
        # Add the nucleotide to the ref allele field
        ref = alt + variant["reference"]
        return pos, ref, alt
    else:
        return variant["start"], variant["reference"], variant["alternative"]


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Returns vcf with exported variants from Alissa"
    )

    parser.add_argument("--json", help="Alissa json path", required=True, type=str)
    parser.add_argument("--fasta", help="Reference fasta file", required=True, type=str)
    parser.add_argument(
        "--output", help="File name of produced vcf", required=True, type=str
    )

    args = parser.parse_args()
    write_vcf(args.json, args.fasta, args.output)
