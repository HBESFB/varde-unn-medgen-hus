import unittest
import os
from hus_alissa_to_vcf import (
    handle_insertion,
    handle_deletion,
    variant_string,
    latest_interpretation,
)

base_folder = os.getcwd()
test_path = os.path.join(base_folder, "test-input")


class Test_handle_insertion(unittest.TestCase):
    def test_reference_is_dot(self):
        variant = {
            "start": "5",
            "reference": ".",
            "chromosome": "1",
            "alternative": "A",
        }
        pos, ref, alt = handle_insertion(variant, os.path.join(test_path, "test.fasta"))
        assert alt == "EA"
        assert pos == "5", ref == "E"

    def test_reference_is_not_dot(self):
        variant = {
            "start": "5",
            "reference": "E",
            "chromosome": "1",
            "alternative": "B",
        }
        pos, ref, alt = handle_insertion(variant, os.path.join(test_path, "test.fasta"))
        assert alt == "B"
        assert pos == "5", ref == "E"


class Test_handle_deletion(unittest.TestCase):
    def test_alt_is_dot(self):
        variant = {
            "start": "5",
            "reference": "E",
            "chromosome": "1",
            "alternative": ".",
        }
        pos, ref, alt = handle_deletion(variant, os.path.join(test_path, "test.fasta"))
        print(pos, ref, alt)
        assert alt == "D"
        assert pos == "4", ref == "DE"

    def test_alt_is_not_dot(self):
        variant = {
            "start": "5",
            "reference": "E",
            "chromosome": "1",
            "alternative": "A",
        }
        pos, ref, alt = handle_deletion(variant, os.path.join(test_path, "test.fasta"))
        print(pos, ref, alt)
        assert alt == "A"
        assert pos == "5", ref == "E"


class Test_variant_string(unittest.TestCase):
    def test_all_inputs(self):
        want = "1\t55\t.\tA\tB\t.\t.\tCLASS=5;YEAR=2024;ALISSA_POS=1:5:E/A\n"
        variant = {
            "start": "5",
            "reference": "E",
            "chromosome": "1",
            "alternative": "A",
            "classification": "5",
            "lastUpdatedOn": "2024/02/21 16:41:00",
        }
        result = variant_string(variant, "55", "A", "B")
        assert want == result

    def test_one_input(self):
        want = "1\t5\t.\tE\tA\t.\t.\tCLASS=5;YEAR=2024\n"
        variant = {
            "start": "5",
            "reference": "E",
            "chromosome": "1",
            "alternative": "A",
            "classification": "5",
            "lastUpdatedOn": "2024/02/21 16:41:00",
        }
        result = variant_string(variant)
        assert want == result

    def test_two_inputs(self):
        want = "1\t55\t.\t.\t.\t.\t.\tCLASS=5;YEAR=2024;ALISSA_POS=1:5:E/A\n"
        variant = {
            "start": "5",
            "reference": "E",
            "chromosome": "1",
            "alternative": "A",
            "classification": "5",
            "lastUpdatedOn": "2024/02/21 16:41:00",
        }
        result = variant_string(variant, "55")
        assert want == result


class Test_latest_interpretation(unittest.TestCase):
    def test_different_day(self):
        input = [
            {
                "start": "5",
                "stop": "6",
                "reference": "E",
                "chromosome": "1",
                "alternative": "A",
                "classification": "5",
                "lastUpdatedOn": "2024/02/21 16:41:00",
            },
            {
                "start": "5",
                "stop": "6",
                "reference": "E",
                "chromosome": "1",
                "alternative": "A",
                "classification": "2",
                "lastUpdatedOn": "2024/02/23 16:41:00",
            },
        ]
        want = [
            {
                "start": "5",
                "stop": "6",
                "reference": "E",
                "chromosome": "1",
                "alternative": "A",
                "classification": "2",
                "lastUpdatedOn": "2024/02/23 16:41:00",
            }
        ]
        output = latest_interpretation(input)
        assert want == output

    def test_different_variants(self):
        input = [
            {
                "start": "6",
                "stop": "6",
                "reference": "E",
                "chromosome": "1",
                "alternative": "A",
                "classification": "5",
                "lastUpdatedOn": "2024/02/21 16:41:00",
            },
            {
                "start": "5",
                "stop": "6",
                "reference": "E",
                "chromosome": "1",
                "alternative": "A",
                "classification": "2",
                "lastUpdatedOn": "2024/02/23 16:41:00",
            },
        ]
        output = latest_interpretation(input)
        assert input == output


if __name__ == "__main__":
    unittest.main()
