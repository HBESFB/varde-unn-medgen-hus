## INCOMPLETE AND DEPRECATED
## INCOMPLETE AND DEPRECATED
## INCOMPLETE AND DEPRECATED
import sys
inputfile = sys.argv[1]

with open("converted.vcf", "w") as vcf_file:
    # Write ## headers to the output file
    vcf_file.write("##fileformat=VCFv4.4\n")
    vcf_file.write('##INFO=<ID=CLASS,Number=1,Type=String,Description="Pathogenicity classification">\n')
    vcf_file.write('##contig=<ID=1,length=249250621>\n')
    vcf_file.write('##contig=<ID=2,length=243199373>\n')
    vcf_file.write('##contig=<ID=3,length=198022430>\n')
    vcf_file.write('##contig=<ID=4,length=191154276>\n')
    vcf_file.write('##contig=<ID=5,length=180915260>\n')
    vcf_file.write('##contig=<ID=6,length=171115067>\n')
    vcf_file.write('##contig=<ID=7,length=159138663>\n')
    vcf_file.write('##contig=<ID=8,length=146364022>\n')
    vcf_file.write('##contig=<ID=9,length=141213431>\n')
    vcf_file.write('##contig=<ID=10,length=135534747>\n')
    vcf_file.write('##contig=<ID=11,length=135006516>\n')
    vcf_file.write('##contig=<ID=12,length=133851895>\n')
    vcf_file.write('##contig=<ID=13,length=115169878>\n')
    vcf_file.write('##contig=<ID=14,length=107349540>\n')
    vcf_file.write('##contig=<ID=15,length=102531392>\n')
    vcf_file.write('##contig=<ID=16,length=90354753>\n')
    vcf_file.write('##contig=<ID=17,length=81195210>\n')
    vcf_file.write('##contig=<ID=18,length=78077248>\n')
    vcf_file.write('##contig=<ID=19,length=59128983>\n')
    vcf_file.write('##contig=<ID=20,length=63025520>\n')
    vcf_file.write('##contig=<ID=21,length=48129895>\n')
    vcf_file.write('##contig=<ID=22,length=51304566>\n')
    vcf_file.write('##contig=<ID=X,length=155270560>\n')
    vcf_file.write('##contig=<ID=Y,length=59373566>\n')
    vcf_file.write('##contig=<ID=MT,length=16569>\n')

    # Write # header
    vcf_file.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n")

    # Process each input line
    with open(inputfile, "r") as tsv:
        next(tsv)
        for line in tsv:
            fields = line.strip().split('\t')
            chrom = fields[0].split(":")[0]
            pos = fields[0].split(":")[1]
            ref = fields[3]
            alt = fields[4]
            info = fields[4]
            
            # Standard exonic snps (These blocks can probably be combined)
            if (fields[4]=="snp" and fields[5]=="exonic"):
                # Ex: c.709C>T
                ref = fields[3].split(">")[0][-1]
                alt = fields[3].split(">")[1]
                info = fields[9]
                vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info}\n")
            # Standard intronic snps
            # Needs manual check because of +- in CDNA nomencalture. Whats this?
            elif(fields[4]=="snp" and fields[5]=="intronic"):
                # Ex: c.3717+10G>T or c.-6-4G>T
                ref = fields[3].split(">")[0][-1]
                alt = fields[3].split(">")[1]
                info = fields[9]
                vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info}\n")

            elif(fields[4]=="snp" and (fields[5]=="UTR3" or fields[5]=="UTR5")):
                # Ex: c.*4C>T or c.-184A>G
                ref = fields[3].split(">")[0][-1]
                alt = fields[3].split(">")[1]
                info = fields[9]
                vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info}\n")
            elif(fields[4]=="insertion"):
                # Ex: c.71_72insACAGCAGCAGCA or c.1016dupA
                if ("dup" in fields[3]):
                    ref = fields[3].split("dup")[1][0]
                    alt = fields[3].split("dup")[1]
                if ("ins" in fields[3]):
                    ref = fields[3].split("dup")[1][0]
                ref = fields[3].split(">")[0][-1]
                alt = fields[3].split(">")[1]
                info = fields[9]
                vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info}\n")
            else:
                pass
                print (line)

            # Write the VCF record to the output file
            #vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tINFO={info_field}\n")
