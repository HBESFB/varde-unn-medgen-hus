import sys, json, pycurl
from datetime import datetime
from io import BytesIO
inputfile = sys.argv[1]

def chunk_list(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]

def query_vep(delins):
    # Data to be sent in the POST request
    data = json.dumps({
        "ids": delins,
        "vcf_string": 1,
        "fields": "id"
    })

    response_buffer = BytesIO()
    curl = pycurl.Curl()

    # Set Curl options
    url = 'http://grch37.rest.ensembl.org/variant_recoder/homo_sapiens'
    curl.setopt(curl.URL, url)
    curl.setopt(curl.POSTFIELDS, data)
    curl.setopt(curl.HTTPHEADER, ['Content-Type: application/json'])
    curl.setopt(curl.WRITEDATA, response_buffer)
    curl.perform()

    # Check if the request was successful
    if curl.getinfo(pycurl.RESPONSE_CODE) == 200:
        response = response_buffer.getvalue().decode('utf-8')
        response_buffer.close()
        # Parse and print the response
        parsed_response = json.loads(response)
        results = {}
        for item in parsed_response:
            for key in item:
                if key == "warnings":
                    continue
                input_value = item[key].get("input")
                vcf_string = item[key].get("vcf_string", [])
                if input_value and vcf_string:
                    results[input_value] = vcf_string[0]
        return results
        curl.close()
    else:
        exit(f"Error: {curl.getinfo(pycurl.RESPONSE_CODE)}")
        curl.close()

with open("converted.vcf", "w") as vcf_file:
    # Write ## headers to the output file
    vcf_file.write("##fileformat=VCFv4.4\n")
    vcf_file.write('##INFO=<ID=CLASS,Number=1,Type=String,Description="Variant classification">\n')
    vcf_file.write('##INFO=<ID=ACMG,Number=.,Type=String,Description="ACMG criteria">\n')
    vcf_file.write('##INFO=<ID=PUBMED,Number=.,Type=String,Description="List of Pubmed IDs">\n')
    vcf_file.write('##INFO=<ID=SVLEN,Number=.,Type=Integer,Description="Structural variant length">\n')
    vcf_file.write('##INFO=<ID=YEAR,Number=1,Type=Integer,Description="Year of interpretation">\n')
    vcf_file.write('##contig=<ID=1,length=249250621>\n')
    vcf_file.write('##contig=<ID=2,length=243199373>\n')
    vcf_file.write('##contig=<ID=3,length=198022430>\n')
    vcf_file.write('##contig=<ID=4,length=191154276>\n')
    vcf_file.write('##contig=<ID=5,length=180915260>\n')
    vcf_file.write('##contig=<ID=6,length=171115067>\n')
    vcf_file.write('##contig=<ID=7,length=159138663>\n')
    vcf_file.write('##contig=<ID=8,length=146364022>\n')
    vcf_file.write('##contig=<ID=9,length=141213431>\n')
    vcf_file.write('##contig=<ID=10,length=135534747>\n')
    vcf_file.write('##contig=<ID=11,length=135006516>\n')
    vcf_file.write('##contig=<ID=12,length=133851895>\n')
    vcf_file.write('##contig=<ID=13,length=115169878>\n')
    vcf_file.write('##contig=<ID=14,length=107349540>\n')
    vcf_file.write('##contig=<ID=15,length=102531392>\n')
    vcf_file.write('##contig=<ID=16,length=90354753>\n')
    vcf_file.write('##contig=<ID=17,length=81195210>\n')
    vcf_file.write('##contig=<ID=18,length=78077248>\n')
    vcf_file.write('##contig=<ID=19,length=59128983>\n')
    vcf_file.write('##contig=<ID=20,length=63025520>\n')
    vcf_file.write('##contig=<ID=21,length=48129895>\n')
    vcf_file.write('##contig=<ID=22,length=51304566>\n')
    vcf_file.write('##contig=<ID=X,length=155270560>\n')
    vcf_file.write('##contig=<ID=Y,length=59373566>\n')
    vcf_file.write('##contig=<ID=MT,length=16569>\n')

    # Write # header
    vcf_file.write("#CHROM\tPOS\tID\tREF\tALT\tQUAL\tFILTER\tINFO\n")

    # Process each input line
    with open(inputfile, "r") as filehandle:
        data = json.load(filehandle)
        delins = []
        for variant in data["molecularVariants"]:
            chrom = variant["chromosome"]
            pos = variant["start"]
            ref = variant["reference"]
            alt = variant["alternative"]
            info = variant["classification"]
            # YEAR is anonymized in in-silico-randomized files for test.
            # This pertains to test files and has no affect on production json
            if variant["lastUpdatedOn"] == '-':
                # Write test entries without YEAR=
                vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info}\n")
            # Alissa does not export insertions and deletions correctly (pr. VCF specification), so we correct it using the VEP API
            # Gather deletions and insertions to query VEP in the end
            elif variant["type"] == "insertion" or variant["type"] == "deletion":
                hgvc = "{0}:{1}".format(variant["transcript"], variant["cNomen"])
                delins.append(hgvc)
            # If common mut, write directly to file
            else:
                date_obj = datetime.strptime(variant["lastUpdatedOn"], "%Y/%m/%d %H:%M:%S")
                year = date_obj.strftime("%Y")
                # Write entry
                vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info};YEAR={year}\n")
        
        # Get results from VEP API, reiterate to write remaining entries
        # Example vcf_string from VEP: "17-43092436-CTAATGTTATTA-C",
        if delins:
            # Size limit for API is 200, but gives i timeout much earlier
            chunk_size = 10
            queries = {}

            # Split the queries into chunks of 10 and process each chunk (because size and timeout)
            for chunk in chunk_list(delins, chunk_size):
                queries.update(query_vep(chunk))

            for variant in data["molecularVariants"]:
                if variant["type"] == "insertion" or variant["type"] == "deletion":
                    hgvc = "{0}:{1}".format(variant["transcript"], variant["cNomen"])
                    if hgvc in queries.keys():
                        vep_string = queries[hgvc]
                        vep_string = vep_string.split('-')
                        chrom = vep_string[0]
                        pos = vep_string[1]
                        ref = vep_string[2]
                        alt = vep_string[3]
                        info = variant["classification"]
                        # Write entry
                        vcf_file.write(f"{chrom}\t{pos}\t.\t{ref}\t{alt}\t.\t.\tCLASS={info};YEAR={year}\n")
                    else:
                        print("{} has no hgvc results from Ensemble VEP api".format(hgvc))


        