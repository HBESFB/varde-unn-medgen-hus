# VARDE-UNN-MedGen-local
This repository contains the implementation of the the VARDE local scope container for the Department of Medical Genetics, UNN. This implementation makes sure exported variant files from UNN-MedGen are validated, encrypted and in-line with current variant file specifications mandated by VARDEdb centrally.

Extendability: The nextflow pipeline is structured in a way which makes it easy to extend to accomodate local scope containers for other health regions in Norway.

Quick Access:
- If you need instructions on usage, see [**Usage Instructions**](#usage)
- If you want to use this implementation as a template for a local pipeline, see [**How to integrate with this repository**](#how-to-integrate-with-this-repository)
- If you are looking for requirement specification or design, navigate to the [**VARDEdb Central Repository**](https://gitlab.com/DPIPE/datasharing/varde/vardeDB)  
If you are looking for the **Current Status** of this implementation, [click here](#todo)

## Usage
The pipeline can be executed in two ways - [Conventionally](#conventional), with docker/podman installed on your computer/server or by running a [GitLab pipeline](#with-gitlab-pipeline) from this repository given you have sufficient priveleges

### With gitlab pipeline
Make sure newly exported files from Alamut and/or Alissa is present in the input directory where the gitlab-runner lives. This defaults to **$HOME/varde-local-input** in the current implementation. Encrypted vcf files are written to **$HOME/varde-local-output**, and are uploaded as a GitLab artifact **Build** -> **Artifacts**.  
In the leftmost menu, click **Build** -> **Pipelines** -> **Run Pipeline**.  

### Conventional 
- Pull a specific release. You can find available releases in **Deploy** -> **Releases**  
```
docker pull registry.gitlab.com/unn-medgen/varde-unn-medgen-local:x.x
```
- Run the container specifying pipeline shorthand ID and a local input directory. Make sure to pull a version from a production branch (you can see available releases in **GitLab -> Releases**) Here is an example:  

```
docker run registry.gitlab.com/unn-medgen/varde-unn-medgen-local:x.x \
  -v $PWD/<inputdir>:/varde/input \
  -v $PWD/<outputdir>:/varde/output \
  nextflow main.nf \
  --pipeline "unn-medgen"
```

The input directory needs to have the correct file types to be processed depending on which pipeline is referenced. See the [overview](#pipeline-and-software-overview) of different pipelines, software and their respective file types used in the different health regions for more information.  

You can also set a different gpg public key directory with --gpg <directory> if you want.  

Quick clarification: This container has nextflow inside running with local scope, it does NOT run nextflow with container orchestration (docker_enabled=True, etc.)

## How to integrate with this repository
If you plan to fork this repository, f.ex to use it as a template for a local VARDE pipeline in a different health region, or to integrate an additional pipeline in this implementation, here is how: 

### Adding a pipeline
- First, make a fork of this repository
- Choose an institution, and fill out the neccessary details in [Pipeline Overview](#pipeline-and-software-overview) in this README. You also need to select a  shorthand / ID - which should be used as pipeline selection parameter and GPG identity (shorthand will also be used as filenames/parameter, so do not use the character "/"). Email should be the person responsible for the key and its password.
- Create a gpg key, see [Obtaining a gpg-key](#obtaining-a-gpg-key) for instructions
- Implement a Nextflow pipeline with a similar structure as one that already exists
  - Make a pipeline-<institution>/ folder with a subfolder processes/
  - Define neccessary processes.nf and workflow in pipeline.nf
  - Include the pipeline in main.nf

### Configuring devops (optional)
For devops funtionality to work, makes sure to:  

- Add a gitlab runner.
- Set **$GITLAB_RUNNER** as an environment variable in environments "development" and "production", referencing its tag. This variable is referenced dynamically in "tags:" in the various devops yml files. You can have different runners for development and production environments. (See [GitLab development policy](#gitlab-development-policy) for more details)
- Automatic testing (test.yml) uses test-input/variants.in-silico-randomized.vcf as input (See [Notes on example files](#notes-on-example-files-test-input) for more information)
- The pipeline needs to be started manually (Start in **Build** -> **Pipelines** -> **Run Pipeline**). (See [Usage: With GitLab Pipeline](#with-gitlab-pipeline) for more information)

## Pipeline and software overview

| Institution | Shorthand ID (--pipeline) | GPG Identity (User, Email) | GPG Public Key File | Variant Export Software | Relevant File Types |
| - | - | - | - | - | - |
|[UNN Medisinsk Genetisk Avdeling](https://unn.no/avdelinger/barne-og-ungdomsklinikken/medisinsk-genetisk-avdeling-tromso)| [UNNAMG](#UNNAMG)| UNNAMG, espen.mikal.robertsen@unn.no| public_gpg_keys/UNNAMG.asc | Alamut, Alissa | vcf, json | 
|[HUS Medisinsk Genetikk ](https://www.helse-bergen.no/avdelinger/laboratorieklinikken/medisinsk-genetikk)| [HUSMGM](#HUSMGM)| HUSMGM, person@email.no| public_gpg_keys/HUSMGM.asc | Alamut, Alissa | vcf, json | 

### Decryption (All pipelines)
If the --decrypt <private_key> parameter is set, the pipeline assumes encrypted input. Inputfiles in production runs should always be encrypted if the container runs outside of the local network

### UNNAMG
The department of Medical Genetics at UNN has two sources of VCF files, Alamut (.vcf) and Alissa (.json).  

#### Alamut
For Alamut, the processing pipeline looks like this:
```mermaid
graph LR;
  inputfiles-->Filter_HG38;
  Filter_HG38-->Filter_ChrY;
  Filter_ChrY-->Filter_ChrM;
  Filter_ChrM-->Convert_5ity;
  Convert_5ity-->Sort_Variants;
  Sort_Variants-->Rename_Chr23;
  Rename_Chr23-->Add_Contig_Id_Headers;
  Add_Contig_Id_Headers-->Validate;
  Validate-->Encrypt
```
*Description**

#### Alissa

### HUSMGM


## Development policy
This repository has two environments, "development" and "production".  
### Development:
A commit to master or any other non-versioned branch NOT in the form of <Major.Minor> will run with environment "development", which performs a SAST, a container build and a test run of the pipeline on dummy data.
### Production: 
A commit to a versioned branch in the form of <Major.Minor> will run with environment "production", which performs SAST, build, test and publish.  
A manual stage "run" at the end starts the pipeline using input present in the folder mounted to /varde/input in the container on the server where the gitlab-runner lives.  
Encrypted output will be available both as an artifact in GitLab and in the directory mounted to /varde/output in the container on the server where the gitlab-runner lives.

# Addendum

## Notes on current gpg implementation
The utilized container ships with gpg 2.4.3. This is how you can obtain and handle your private key pair:

### Obtaining a gpg-key
Start the container in interactive mode mounting a gpg directory, and generate the key pair:

```
mkdir gpg
docker run -it -v $PWD/gpg:/gpg registry.gitlab.com/unn-medgen/varde-unn-medgen-local:latest /bin/bash
gpg --gen-key
````
**Imporant**: 
- Input **Real Name** and **Email address** as prompted, where **Real Name** matches your respective --pipeline parameter (**important**). **Email address** should be the email address of the person who is responsible for the key (but can also be arbitrary)     
See [Pipeline Overview](#pipeline-and-software-overview) for an overview.  
- Set a password and keep the password safe.   

Export keys to the mounted directory:

```
gpg --output /gpg/public.asc --armor --export <email>
gpg --output /gpg/private.asc --armor --export-secret-key <email>
```
Rename the public key "public.asc" to the same as your respective institutions shorthand ID (pipeline-parameter.asc) (See [Pipeline Overview](#pipeline-and-software-overview)), for example:
```
mv /gpg/public.asc /gpg/UNNAMG.asc
```
Exit the container. The key pair should be present in the gpg directory.  
Move **ONLY** the public key to the public_gpg_keys/ folder, as Nextflow will look for it here and match it with the pipeline that is run. 
Store the private key and its password somewhere safe

### Encrypting files with obtained key-pair (This is performed automatically by Nextflow)

Import key to keychain:
```
gpg --import <pipeline>.asc
```
Encrypt vcf file
```
gpg -e -r <email> variants.vcf
```

## Notes on example files (test-input)
The directory test-input/ contains a subset of production VCF and TSV files from UNN-MedGen where variants have been randomized and obfuscated in-silico to ensure complete anonymity. This script will not be made public, but can be obtained upon request from [espen.mikal.robertsen@unn.no](mailto:espen.mikal.robertsen@unn.no)

##  TODO
- [x] Add all neccessary software/data etc. to dockerfile
- [x] Obtain test-input for Alissa / Alamut
  - [x] Anonymize and sync to repo
- [x] Implement basic CiCd components
  - [x] Make manual pipeline (yml when:manual)
  - [x] Setup environments and variables
  - [x] Add job artifact for encrypted output
- [x] Implement basic Nextflow pipeline structure with pipeline selection
  - [x] Make nf parameter for health region, dynamically referencing specific pipeline. 
    - [ ] Investigate whether conflicting namespaces can be problematic with current design
    - [ ] Implement nextflow UNN-MedGen: filters/transformation
    - [x] Implement nexflow UNN-MedGen: validation
    - [x] Implement nextflow UNN-MedGen: gpg-sign
      - [ ] Add support for encrypted input (If gitlab server is outside a safe network)
    - [ ] Implement neccessary output metrics, log, etc.
    - [ ] Merge Alissa / Alamut input ?
    - [ ] Alamut: Convert string based classifications to numbers ?